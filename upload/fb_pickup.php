<?php

// Configuration
if (file_exists('config.php')) {
	require_once('config.php');
}

require_once(DIR_SYSTEM . 'library/db.php');

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

$fb_settings = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `group`='fb_mobile' AND store_id=". (int)$_REQUEST['store_id']);

if ($fb_settings->num_rows)
{
	$app_id = '';
	$app_secret = '';
	foreach ($fb_settings->rows as $row)
	{
		if ($row['key'] == 'app_id')
		{
			$app_id = $row['value'];
		}
		elseif ($row['key'] == 'app_secret')
		{
			$app_secret = $row['value'];
		}
	}

	require_once('./system/library/facebook/src/facebook.php');

	$config = array();
	$config['appId'] 	= $app_id;
	$config['secret'] 	= $app_secret;

	$facebook = new Facebook($config);
	$user = $facebook->getUser();
}

	header("Location: ". $_SERVER['HTTP_REFERER']);
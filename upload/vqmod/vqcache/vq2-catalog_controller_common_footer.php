<?php  
class ControllerCommonFooter extends Controller {
	protected function index() {
		$this->language->load('common/footer');

		if($this->isVisitorMobile()) {				
			$this->language->load('common/header');
		}
		$this->language->load('omf/common');		
		
			
		
		$this->data['text_information'] = $this->language->get('text_information');
		$this->data['text_service'] = $this->language->get('text_service');
		$this->data['text_extra'] = $this->language->get('text_extra');
		$this->data['text_contact'] = $this->language->get('text_contact');
		$this->data['text_return'] = $this->language->get('text_return');
    	$this->data['text_sitemap'] = $this->language->get('text_sitemap');
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$this->data['text_voucher'] = $this->language->get('text_voucher');
		$this->data['text_affiliate'] = $this->language->get('text_affiliate');
		$this->data['text_special'] = $this->language->get('text_special');
		$this->data['text_account'] = $this->language->get('text_account');
		$this->data['text_order'] = $this->language->get('text_order');
		$this->data['text_wishlist'] = $this->language->get('text_wishlist');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');
		//Required by the stuff coming from the header.php
		$this->data['text_language'] = $this->language->get('text_language');
    	$this->data['text_currency'] = $this->language->get('text_currency');
		$this->data['text_home'] = $this->language->get('text_home');
		$this->data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		$this->data['text_cart'] = $this->language->get('text_cart');
		$this->data['text_search'] = $this->language->get('text_search');
		$this->data['text_checkout'] = $this->language->get('text_checkout');
		$this->data['text_welcome'] = sprintf($this->language->get('text_welcome'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
		$this->data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
		
		//For the contact submenu
		$this->language->load('information/contact');
		$this->data['text_address'] = str_replace(':','',$this->language->get('text_address'));
		$this->data['text_enquiry'] = str_replace(':','',$this->language->get('entry_enquiry'));
		$this->data['text_call'] = $this->language->get('text_call');		
		
		//OMF specific
		$this->data['text_all_categories'] = $this->language->get('text_all_categories');
		$this->data['text_top'] = $this->language->get('text_top');
		$this->data['text_view'] = $this->language->get('text_view');		
		$this->data['text_mobile'] = $this->language->get('text_mobile');
		$this->data['text_standard'] = $this->language->get('text_standard');
				
		//Switch back and forth mobile/full site link
		$this->data['switch_mobile'] = $this->url->link('common/switch_mobile') ;		
		
		if (isset($this->request->get['filter_name'])) {
			$this->data['filter_name'] = $this->request->get['filter_name'];
		} else {
			$this->data['filter_name'] = '';
		}
		
		$this->data['home'] = $this->url->link('common/home');
		$this->data['wishlist'] = $this->url->link('account/wishlist');
		$this->data['logged'] = $this->customer->isLogged();
		$this->data['account'] = $this->url->link('account/account', '', 'SSL');
		$this->data['cart'] = $this->url->link('checkout/cart');
		$this->data['info'] = $this->url->link('information/info_page_list');
		$this->data['checkout'] = $this->url->link('checkout/login', '', 'SSL');
		
		$this->data['address'] = urlencode(str_replace('\n', '+', $this->config->get('config_address')));
    	$this->data['telephone'] = $this->config->get('config_telephone');
		
		$this->data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');
			
		
		$this->load->model('catalog/information');
		
		$this->data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$this->data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
    	}

		$this->data['contact'] = $this->url->link('information/contact');
		$this->data['return'] = $this->url->link('account/return/insert', '', 'SSL');
    	$this->data['sitemap'] = $this->url->link('information/sitemap');
		$this->data['manufacturer'] = $this->url->link('product/manufacturer');
		$this->data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$this->data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$this->data['special'] = $this->url->link('product/special');
		$this->data['account'] = $this->url->link('account/account', '', 'SSL');
		$this->data['order'] = $this->url->link('account/order', '', 'SSL');
		$this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');		

		$this->data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));
		//Trigger only if client is on a mobile device		
		if($this->isVisitorMobile()) { 
			// Menu			
			//$this->data['routeDebug'] = $this->request->get['route'];			
			$route = empty($this->request->get['route']) ? 'common/home' : $this->request->get['route'];			
			$this->data['route'] = $route;			
			
			if($route == 'common/home' ) { //If on homepage show the categories. Else hide them.
				$this->load->model('catalog/category');
				$this->load->model('catalog/product');				
				
				$this->data['categories'] = array();
							
				$categories = $this->model_catalog_category->getCategories(0);
				
				foreach ($categories as $category) {
					//if ($category['top']) {
						$children_data = array();
						
						$children = $this->model_catalog_category->getCategories($category['category_id']);
						
						foreach ($children as $child) {
							$data = array(
								'filter_category_id'  => $child['category_id'],
								'filter_sub_category' => true	
							);		
								
							#$product_total = $this->model_catalog_product->getTotalProducts($data);
											
							$children_data[] = array(
								'name'  => $child['name'] /*. ' (' . $product_total . ')'*/,
								'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])	
							);					
						}
						
						// Level 1
						$this->data['categories'][] = array(
							'name'     => $category['name'],
							'children' => $children_data,
							'column'   => $category['column'] ? $category['column'] : 1,
							'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
						);						
						
					//}
				}
				
				$this->data['all_categories'] = $this->url->link('common/all_categories');						
			}
			
			$this->children = array(
				'module/language',
				'module/currency',			
			);						
		}		
			
		
		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');
	
			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];	
			} else {
				$ip = ''; 
			}
			
			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];	
			} else {
				$url = '';
			}
			
			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];	
			} else {
				$referer = '';
			}
						
			$this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);
		}		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/footer.tpl';
		} else {
			$this->template = 'default/template/common/footer.tpl';
		}
		
		$this->render();
	}
}
?>
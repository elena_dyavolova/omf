<?php
// Heading
$_['heading_title']       = 'FB login Mobile';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module fb mobile!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['facebook_app_id']     = 'Facebook App Id';
$_['facebook_app_secret'] = 'Facebook App secret';

// Entry
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_count']    	  = 'Product Count:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module fb mobile!';
?>
if(document.readyState !== "complete") {  
	document.addEventListener("DOMContentLoaded", function () {  				
		if(topNavAnchors = document.querySelectorAll("#primary .nav > li > a")) {
			var topNavLi = document.querySelectorAll("#primary .nav > li ");
					
			var j = 0;	
			
			for (var i = 0; i < topNavAnchors.length; i++) {   			
				if(topNavLi[i].getElementsByTagName("ul").length != 0) {
					j++;
					topNavAnchors[i].onclick = function() {	
						return false;
					}; 			
				}
			}
		}
		
		if(languageForm = document.forms["language"]) {
			var languageSelect = languageForm.getElementsByTagName("select");		
			
			languageSelect[0].onchange= function() {
				languageForm.submit();
			};
		}
		
		if(currencyForm = document.forms["currency"]) {
			var currencySelect = currencyForm.getElementsByTagName("select");				
			
			currencySelect[0].onchange= function() {
				currencyForm.submit();
			};		
		}
		
		if(!supportsSVG()) {			
			document.getElementsByTagName("body")[0].className += " no-svg";			
		}
	}, false);
}

function supportsSVG() {
    return !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', "svg").createSVGRect;
}
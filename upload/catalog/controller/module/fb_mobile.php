<?php
class ControllerModuleFbMobile extends Controller {
	protected function index($setting) {

		$loginUrl = '';

		if ( ! $this->customer->isLogged())
		{
			require_once(DIR_SYSTEM . 'library/facebook/src/facebook.php');

			$config = array();
			$config['appId'] 	= $this->config->get('app_id');
			$config['secret'] 	= $this->config->get('app_secret');
			$config['url'] 		= $this->config->get('config_url');

			$facebook = new Facebook($config);
			$user = $facebook->getUser();

			if ($user)
			{
				try
				{
					$user_profile = $facebook->api('/me');

					if ( ! $this->loginFbUser($user_profile['email']))
					{
						$this->registerFbUser($user_profile);
					}
				}
				catch (FacebookApiException $e)
				{
					$user = null;
				}
			}

			if ( ! $user)
			{
				$loginUrl = $facebook->getLoginUrl(array(
														 //'redirect_uri'=> $config['url'] .'fb_pickup.php?store_id='. $this->config->get('config_store_id'),
														 'redirect_uri'=> 'http://localhost/fb_pickup.php?store_id='. $this->config->get('config_store_id'),
														 'scope' => 'email'
														 )
												   );
			}
		}

		$this->language->load('module/category_mobile');

		$this->data['loginUrl'] 		= $loginUrl;

		$this->template = 'omf/template/module/fb_mobile.tpl';

		$this->render();
  	}

	function registerFbUser($user)
	{
		$data['firstname'] 	= $user['first_name'];
		$data['lastname'] 	= $user['last_name'];
		$data['email'] 		= $user['email'];
		$data['telephone'] 	= '';
		$data['fax'] 		= '';
		$data['company'] 	= '';
		$data['company_id'] = '';
		$data['tax_id'] 	= '';
		$data['password'] 	= md5(time());
		$data['address_1'] 	= '';
		$data['address_2'] 	= '';
		$data['city'] 		= '';
		$data['postcode'] 	= '';
		$data['zone_id'] 	= $this->config->get('config_zone_id');
		$data['country_id'] = $this->config->get('config_country_id');

		$this->load->model('account/customer');
		if ($this->model_account_customer->addCustomer($data))
		{
			return $this->loginFbUser($user['email']);
		}
		else
		{
			return false;
		}
	}

	function loginFbUser($email)
	{
		return $this->customer->login($email, '', TRUE);
	}
}
?>
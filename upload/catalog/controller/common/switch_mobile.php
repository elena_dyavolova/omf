<?php  
class ControllerCommonSwitchMobile extends Controller {
	public function index() {				
		if (isset($request->cookie['mobile'])) {
			if ($request->cookie['mobile'] ==' true') {
				setcookie('mobile', 'false', time() + 3600 * 24 * 365, '/');
			} else {
				setcookie('mobile', 'true', time() + 3600 * 24 * 365, '/');
			}
		} else {
			if($this->isVisitorMobile()) {
				setcookie('mobile', 'false', time() + 3600 * 24 * 365, '/');
			} else {
				setcookie('mobile', 'true', time() + 3600 * 24 * 365, '/');
			}
		}			
		
		if(isset($_SERVER['HTTP_REFERER'])) {			
			if(strrpos($_SERVER['HTTP_REFERER'],"all_categories") === false) {
				$route = $_SERVER['HTTP_REFERER'];
			} else {
				$route = $this->url->link('common/home') ;
			}
		} else {
			$route = $this->url->link('common/home') ;
		}
		
		$this->redirect($route);
	}
}
?>